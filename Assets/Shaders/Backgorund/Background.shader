Shader "Custom/Background"
{
    Properties
    {
        _BackGround("Background", 2D) = "white" {}
        _TopBackground ("Top background", 2D) = "white" {}
        _BotBackground ("Bottom background", 2D) = "white" {}
        _DisTex ("Distorsion tex", 2D) = "white" {}

        [Space(10)]
        //_StarsColor("StarsColor", color) = (1, 1, 1, 1)
        _BackgroundColor("Background color", color) = (1, 1, 1, 1)
        _DisSpeed ("Distorsion speed", Range(-1, 1)) = 0
        _DisValue ("Distorsion value", Range(0.1, 20)) = 1
        _SpeedTop ("Top background speed", Range(-10, 10)) = 1
        _SpeedBot ("Bot background speed", Range(-10, 10)) = 1
        _Glow("Intensity", Range(1, 4)) = 1
        _RangeColor ("Range color", Range (1, 10)) = 1

        [HideInInspector]_ColorR ("Color red", float) = 1
        [HideInInspector]_ColorG ("Color green", float) = 1
        [HideInInspector]_ColorB ("Color blue", float) = 1
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _BackGround;
            float4 _BackGround_ST;
            sampler2D _DisTex;
            float _DisSpeed;
            float _DisValue;
            float4 _BackgroundColor;
            float _ColorR;
            float _ColorG;
            float _ColorB;
            float _RangeColor;

            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _BackGround);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 distorsion = tex2D(_DisTex, i.uv + (_Time.y * _DisSpeed));
                i.uv.x += distorsion / _DisValue;
                i.uv.y += distorsion / _DisValue;

                // sample the texture
                fixed4 col = tex2D(_BackGround, i.uv);
                return col * (float4(_ColorR, _ColorG, _ColorB, 0) / _RangeColor) * distorsion;
            }
            ENDCG
        }
        Pass
        {
            Tags {"Queue"="Transparent"}
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uvv : TEXCOORD01;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uvv : TEXCOORD01;
                float4 vertex : SV_POSITION;
            };
            sampler2D _TopBackground;
            sampler2D _BotBackground;
            
            float4 _TopBackground_ST;
            float4 _BotBackground_ST;

            float _SpeedTop;
            float _SpeedBot;
            float _Glow;
            float _ColorR;
            float _ColorG;
            float _ColorB;

            v2f vert (appdata v)
            {
                v2f o;

                v.uv.y -= _Time.y * _SpeedTop;
                v.uvv.y -= _Time.y * _SpeedBot;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _TopBackground);
                o.uvv = TRANSFORM_TEX(v.uvv, _BotBackground);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 top = tex2D(_TopBackground, i.uv);
                fixed4 bot = tex2D(_BotBackground, i.uvv);

                fixed4 stars = top * bot;

                return stars * float4(_ColorR, _ColorG, _ColorB, 1) * _Glow;
            }
            ENDCG
        }
    }
}