Shader "Custom/Ships"
{
    Properties
    {
        _EffectTex ("Effect texture", 2D) = "white" {}
        _Tilling("Size", Range(1, 20)) = 1
        _Color("Color", color) = (1, 1, 1, 1)
        _Speed("Speed", Range(-1, 1)) = 0

        [HideInInspector]_ColorR("Color red", float) = 1
        [HideInInspector]_ColorG("Color green", float) = 1
        [HideInInspector]_ColorB("Color blue", float) = 1
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _EffectTex;
            float4 _EffectTex_ST;
            float4 _Color;

            float _Tilling;
            float _Speed;
            float _ColorR;
            float _ColorG;
            float _ColorB;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = ComputeScreenPos(o.vertex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 screenPos = ComputeScreenPos(i.vertex).xy / _ScreenParams.xy;
                screenPos.x += _Time.y * _Speed;

                float4 mask = tex2D(_EffectTex, screenPos * _Tilling);

                return mask * _Color * 2;
            }
            ENDCG
        }/*
        Pass
        {
            Tags { "RenderType"="Transparent" }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }*/
    }
}
