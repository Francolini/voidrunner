using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Companion : MonoBehaviour
{
    private PlayerController pc;
    private bool canAttack;
    public GameObject bulletPrefab, sfxPrefab, player;

    // Start is called before the first frame update
    void Awake()
    {
        canAttack = true;
        pc = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        print(canAttack);
        if (canAttack && pc.attack)
        {
            StartCoroutine("Attack");
        }
    }

    private IEnumerator Attack()
    {
        canAttack = !canAttack;
        GameObject bullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);

        if(sfxPrefab != null)
        {
            GameObject playerShoot = Instantiate(sfxPrefab, transform.position, transform.rotation);
            playerShoot.GetComponent<SFX>().sound = SFX.Sounds.playerShoot;
        }

        yield return new WaitForSeconds(.3f);
        canAttack = !canAttack;
    }

    private void OnEnable()
    {
        StopCoroutine("Attack");
        canAttack = true;
    }
}
