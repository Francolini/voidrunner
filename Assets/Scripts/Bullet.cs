using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed, destroyTime;
    public GameObject bulletEnemyParts, bulletPlayerParts;

    private void Awake()
    {
        Destroy(gameObject, destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("PlayerBullet") && gameObject.tag.Equals("EnemyBullet"))
        {
            GameObject bPP = Instantiate(bulletEnemyParts, collision.ClosestPoint(transform.position), transform.rotation);
            Destroy(bPP, 1f);

            GameObject bEP = Instantiate(bulletPlayerParts, transform.position, transform.rotation);
            Destroy(bEP, 1f);

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }

        if (collision.tag.Equals("Enemy") && gameObject.tag.Equals("PlayerBullet") || collision.tag.Equals("EnemyTraining") && gameObject.tag.Equals("PlayerBullet"))
        {
            GameObject bPP = Instantiate(bulletPlayerParts, collision.ClosestPoint(transform.position), transform.rotation);
            Destroy(bPP, 1f);

            Destroy(gameObject);
        }

        if (collision.tag.Equals("Player") && gameObject.tag.Equals("EnemyBullet"))
        {
            GameObject bEE = Instantiate(bulletEnemyParts, collision.ClosestPoint(transform.position), transform.rotation);
            Destroy(bEE, 1f);

            Destroy(gameObject);
        }
    }
}
