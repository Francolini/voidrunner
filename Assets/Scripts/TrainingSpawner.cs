using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingSpawner : MonoBehaviour
{
    public GameObject laserCoords, enemyCoords, companionCoords, laserPrefab, companionPrefab, enemyPrefab;
    private bool canSpawn;

    // Start is called before the first frame update
    void Start()
    {
        GameObject laserSpawn = Instantiate(laserPrefab, laserCoords.transform.position, laserCoords.transform.rotation);
        GameObject enemySpawn = Instantiate(enemyPrefab, enemyCoords.transform.position, enemyCoords.transform.rotation);
        GameObject companionSpawn = Instantiate(companionPrefab, companionCoords.transform.position, companionCoords.transform.rotation);

        canSpawn = true;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject laserTraining = GameObject.FindGameObjectWithTag("LaserTraining");

        if(laserTraining == null && canSpawn)
        {
            StartCoroutine(Spawn(laserPrefab, laserCoords));
        }

        GameObject enemyTraining = GameObject.FindGameObjectWithTag("EnemyTraining");

        if (enemyTraining == null && canSpawn)
        {
            StartCoroutine(Spawn(enemyPrefab, enemyCoords));
        }
        GameObject companionTraining = GameObject.FindGameObjectWithTag("CompanionsTraining");

        if (companionTraining == null && canSpawn)
        {
            StartCoroutine(Spawn(companionPrefab, companionCoords));
        }
    }

    private IEnumerator Spawn(GameObject go, GameObject coord)
    {
        canSpawn = !canSpawn;

        yield return new WaitForSeconds(2);

        GameObject laserSpawn = Instantiate(go, coord.transform.position, coord.transform.rotation);
        canSpawn = !canSpawn;
    }
}
