using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private PlayerController pc;

    // Start is called before the first frame update
    void Awake()
    {
        GameObject target = GameObject.FindGameObjectWithTag("Player");

        if (target != null)
        {
            pc = target.GetComponent<PlayerController>();
        }

        if (gameObject.tag.Equals("LaserPowerUp") || gameObject.tag.Equals("CompanionPU"))
        {
            Destroy(gameObject, 6);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag.Equals("LaserPowerUp") || gameObject.tag.Equals("CompanionPU"))
        {
            transform.Translate(new Vector2(0, -1) * Time.deltaTime * Random.Range(3, 10), Space.World);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (gameObject.tag.Equals("LaserPowerUp") || gameObject.tag.Equals("LaserTraining"))
            {
                pc.ActivateLaser();
            }

            if (gameObject.tag.Equals("CompanionPU") || gameObject.tag.Equals("CompanionsTraining"))
            {
                pc.ActivateCompanions();
            }

            Destroy(gameObject);
        }
    }
}
