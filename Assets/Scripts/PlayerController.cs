﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private PlayerInput input;
    private ScoreController scController;

    private enum Weapons { gun, laser };
    private Weapons weapon;

    private Vector2 moveInput, rotateInput;

    private LineRenderer lineRenderer;

    private GameObject laser;

    private float limitX, limitY, cadency, laserAmmo;
    private bool canAttack, dead, haveCompanions;
    private int scoreMultiplier, enemyCounter;

    public int score, health;

    public GameObject bulletPrefab, turret, laserPrefab, laserEndGO, laserGO, laserParts, laserHitParts, explosionPrefab, gameController, sfxPrefab, scoreMultiplierPrefab, scoreMultiplierPrefabPoint, scoreMultGO, companionsGO, comp01, comp02, scoreController;
    public Text scoreText, scoreShadowText, totalScore, totalScoreShadow, healthText, healthTextShadow, laserText, laserShadowText, scMult, scSMult;

    public float speed, limitRotation;
    public bool attack;

    public LayerMask enemies;

    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.DeleteAll();
        input = GetComponent<PlayerInput>();
        scController = scoreController.GetComponent<ScoreController>();

        limitX = 4.3f;
        limitY = 8.2f;
        cadency = .12f;

        attack = false;
        canAttack = true;
        dead = false;
        haveCompanions = false;

        speed = 9f;
        limitRotation = 0.3f;

        weapon = Weapons.gun;

        score = scController.GetScore();
        scoreMultiplier = 1;
        laserAmmo = 100;
        health = 100;

        scoreText.text = "SC: " + score.ToString();
        scoreShadowText.text = "SC: " + score.ToString();

        healthText.text = "HP: " + health.ToString();
        healthTextShadow.text = "HP: " + health.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(health > 0)
        {
            moveInput = input.actions["Move"].ReadValue<Vector2>();
            rotateInput = input.actions["Rotate"].ReadValue<Vector2>();

            LimitPosition();

            transform.Translate(new Vector2(moveInput.x, moveInput.y) * Time.deltaTime * speed, Space.World);

            if (rotateInput.x >= limitRotation || rotateInput.y >= limitRotation || rotateInput.x <= -limitRotation || rotateInput.y >= limitRotation || rotateInput.x <= -limitRotation || rotateInput.y <= -limitRotation || rotateInput.x >= limitRotation || rotateInput.y <= -limitRotation)
            {
                turret.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up * rotateInput.x + Vector3.left * rotateInput.y);
            }

            switch (weapon)
            {
                case Weapons.gun:

                    if (input.actions["Attack"].triggered)
                    {
                        attack = !attack;
                    }

                    if (attack && canAttack)
                    {
                        StartCoroutine("Attack");
                    }

                    break;

                case Weapons.laser:

                    if (laserAmmo <= 0)
                    {
                        laserParts.SetActive(false);
                        Destroy(laser);
                        ActivateGun();
                    }
                    else
                    {
                        if (input.actions["Attack"].triggered)
                        {
                            attack = !attack;

                            GameObject laserSound = Instantiate(sfxPrefab, transform.position, transform.rotation);

                            if (attack)
                            {
                                laserSound.GetComponent<SFX>().sound = SFX.Sounds.laser;
                                laserSound.name = "LaserPlayer";
                            }
                            else
                            {
                                Destroy(GameObject.Find("LaserPlayer"));
                            }
                        }

                        if (attack)
                        {
                            if(GameObject.Find("LaserPlayer") == null)
                            {
                                GameObject laserSound = Instantiate(sfxPrefab, transform.position, transform.rotation);

                                laserSound.GetComponent<SFX>().sound = SFX.Sounds.laser;
                                laserSound.name = "LaserPlayer";
                            }
                            
                            laserParts.SetActive(true);

                            laserAmmo -= Time.deltaTime * Random.Range(10, 18);

                            laserText.text = laserAmmo.ToString("0") + "%";
                            laserShadowText.text = laserAmmo.ToString("0") + "%";

                            if (lineRenderer == null)
                            {
                                laser = Instantiate(laserPrefab, transform.position, transform.rotation);
                                lineRenderer = laser.GetComponent<LineRenderer>();
                            }

                            lineRenderer.SetPosition(0, laserEndGO.transform.position);
                            lineRenderer.SetPosition(1, transform.position);

                            if (Vector3.Distance(transform.position, laserEndGO.transform.position) < 20)
                            {
                                laserEndGO.transform.Translate(Vector3.right * Time.deltaTime * 30);
                            }

                            RaycastHit2D hit = Physics2D.Linecast(transform.position, laserEndGO.transform.position, enemies);

                            if (hit.collider != null)
                            {
                                if (hit.collider.gameObject.tag.Equals("EnemyBullet"))
                                {
                                    Destroy(hit.collider.gameObject);
                                }

                                if (hit.collider.gameObject.tag.Equals("Enemy") || hit.collider.gameObject.tag.Equals("EnemyTraining"))
                                {
                                    Vector3 hitPoint = new Vector3(hit.point.x, hit.point.y, transform.position.z);

                                    lineRenderer.SetPosition(0, hitPoint);
                                    laserEndGO.transform.position = hitPoint;
                                    laserHitParts.SetActive(true);

                                    EnemyController ec = hit.collider.gameObject.GetComponent<EnemyController>();
                                    ec.TakeDamage(Time.deltaTime * Random.Range(250, 351));
                                }
                            }
                            else
                            {
                                laserHitParts.SetActive(false);
                            }
                        }
                        else
                        {
                            laserEndGO.transform.position = transform.position;
                            laserHitParts.SetActive(false);
                            laserParts.SetActive(false);

                            Destroy(laser);
                        }
                    }                    

                    break;
            }
        }
        else
        {
            if (!dead)
            {
                dead = true;
                health = 0;
                scoreController.GetComponent<ScoreController>().score = score;

                laserGO.SetActive(false);

                if (laser != null)
                {
                    laserParts.SetActive(false);
                    Destroy(laser);
                }

                Destroy(GameObject.Find("LaserPlayer"));

                totalScore.text = score.ToString();
                totalScoreShadow.text = score.ToString();

                healthText.text = "ERROR!!";
                healthTextShadow.text = "ERROR!!";

                scoreText.text = "ERROR!!";
                scoreShadowText.text = "ERROR!!";

                if (!gameController.GetComponent<PrincipalMenu>().canSlow)
                {
                    gameController.GetComponent<PrincipalMenu>().TurnOnOffSlow();
                }

                GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
                Destroy(explosion, 2f);

                GameObject explosionSound = Instantiate(sfxPrefab, transform.position, transform.rotation);
                explosionSound.GetComponent<SFX>().sound = SFX.Sounds.explosion;

                Destroy(gameObject);

                PrincipalMenu pm = gameController.GetComponent<PrincipalMenu>();
                pm.OpenRetryMenu();
            }
        }
    }

    private void LimitPosition()
    {
        if (transform.position.x <= -limitX && moveInput.x < 0 || transform.position.x >= limitX && moveInput.x > 0)
        {
            moveInput.x = 0;
        }

        if (transform.position.y <= -limitY + 4 && moveInput.y < 0 || transform.position.y >= limitY && moveInput.y > 0)
        {
            moveInput.y = 0;
        }
    }

    public void AddScore(int points)
    {
        if (enemyCounter < 5)
        {
            enemyCounter++;
        }

        if(enemyCounter >= 5 && scoreMultiplier < 4)
        {
            enemyCounter = 0;
            scoreMultiplier++;

            GameObject spawnScoreMultiplier = Instantiate(scoreMultiplierPrefab, scoreMultiplierPrefabPoint.transform.position, scoreMultiplierPrefabPoint.transform.rotation);
            spawnScoreMultiplier.transform.parent = GameObject.Find("Canvas").transform;
            Text sm = spawnScoreMultiplier.GetComponentInChildren<Text>();
            sm.text = "X" + scoreMultiplier.ToString();

            scoreMultGO.SetActive(true);
            scMult.text = "X" + scoreMultiplier.ToString();
            scSMult.text = "X" + scoreMultiplier.ToString();
        }

        score += points * scoreMultiplier;

        scoreText.text = "SC: " + score.ToString();
        scoreShadowText.text = "SC: " + score.ToString();
    }

    public void ActivateLaser()
    {
        GameObject powerUp = Instantiate(sfxPrefab, transform.position, transform.rotation);
        powerUp.GetComponent<SFX>().sound = SFX.Sounds.powerUp;

        laserGO.SetActive(true);
        weapon = Weapons.laser;
        laserAmmo = 100;

        laserText.text = laserAmmo.ToString("0") + "%";
        laserShadowText.text = laserAmmo.ToString("0") + "%";
    }

    public void ActivateGun()
    {
        Destroy(GameObject.Find("LaserPlayer"));

        laserHitParts.SetActive(false);
        laserParts.SetActive(false);
        laserGO.SetActive(false);

        weapon = Weapons.gun;
    }

    public void ActivateCompanions()
    {
        GameObject powerUp = Instantiate(sfxPrefab, transform.position, transform.rotation);
        powerUp.GetComponent<SFX>().sound = SFX.Sounds.powerUp;

        if (!haveCompanions)
        {
            StartCoroutine("Companions");
        }
        else
        {
            StopCoroutine("Companions");
            StartCoroutine("Companions");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            Destroy(GameObject.Find("LaserPlayer"));
            Destroy(collision.gameObject);
            health -= 500;

            healthText.text = "ERROR!!";
            healthTextShadow.text = "ERROR!!";
        }

        if (collision.tag.Equals("EnemyBullet"))
        {
            Destroy(collision.gameObject);

            health -= Random.Range(5, 11);

            if (!gameObject.name.Equals("Player") && health <= 0)
            {
                health = 1;
            }

            if (scoreMultiplier != 1)
            {
                scoreMultGO.SetActive(false);
                GameObject spawnScoreMultiplier = Instantiate(scoreMultiplierPrefab, scoreMultiplierPrefabPoint.transform.position, scoreMultiplierPrefabPoint.transform.rotation);
                spawnScoreMultiplier.transform.parent = GameObject.Find("Canvas").transform;
                Text sm = spawnScoreMultiplier.GetComponentInChildren<Text>();
                sm.text = "X1";
            }

            enemyCounter = 0;
            scoreMultiplier = 1;

            healthText.text = "HP: " + health.ToString();
            healthTextShadow.text = "HP: " + health.ToString();
        }
    }

    private IEnumerator Attack()
    {
        canAttack = !canAttack;
        GameObject bullet = Instantiate(bulletPrefab, new Vector3(turret.transform.position.x, turret.transform.position.y, turret.transform.position.z + .1f), turret.transform.rotation);

        GameObject playerShoot = Instantiate(sfxPrefab, transform.position, transform.rotation);
        playerShoot.GetComponent<SFX>().sound = SFX.Sounds.playerShoot;

        yield return new WaitForSeconds(cadency);

        canAttack = !canAttack;
    }

    private IEnumerator Companions()
    {
        companionsGO.SetActive(true);
        haveCompanions = true;

        yield return new WaitForSeconds(20);

        companionsGO.SetActive(false);
        haveCompanions = false;
    }
}
