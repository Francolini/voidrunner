using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PrincipalMenu : MonoBehaviour
{
    private float speed, pauseScaleTime;
    public bool canSlow;

    public GameObject game, reward, retry, spawner, cam, player, player2, resume, pause, btm, gameHUD;
    private AudioSource music;
    private PlayerController pc, pc2;

    private bool isRewarded;

    private void Start()
    {
        pauseScaleTime = 1;
        pc = player.GetComponent<PlayerController>();
        pc2 = player2.GetComponent<PlayerController>();

        if (!gameObject.name.Equals("MenuCanvas"))
        {
            music = cam.GetComponent<AudioSource>();
            Time.timeScale = 1;
            speed = 2.5f;
            canSlow = false;
        }

        isRewarded = false;
    }

    private void Update()
    {
        if (canSlow && !gameObject.name.Equals("MenuCanvas"))
        {
            Time.timeScale -= Time.deltaTime * speed;
            music.volume -= Time.deltaTime / 1.75f;
            
            if(Time.timeScale <= .1f)
            {
                music.volume = .5f;
                Time.timeScale = 1;
                TurnOnOffSlow();
            }
        }
    }

    public void TurnOnOffSlow()
    {
        canSlow = !canSlow;
    }

    public void Play()
    {
        SceneManager.LoadScene("Game");
    }

    public void Pause()
    {
        if (!pc.attack && !pc2.attack)
        {
            resume.SetActive(true);
            btm.SetActive(true);
            gameHUD.SetActive(false);
            pause.SetActive(false);

            music.Pause();
            pauseScaleTime = Time.timeScale;
            canSlow = false;
            Time.timeScale = 0;
        }
    }

    public void Resume()
    {
        music.Play();
        Time.timeScale = pauseScaleTime;

        if(Time.timeScale < 1)
        {
            canSlow = true;
        }
    }

    public void Training()
    {
        SceneManager.LoadScene("Training");
    }

    public void Menu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OpenRetryMenu()
    {
        StartCoroutine("OpenRewardMenyCoroutine");
    }

    private IEnumerator OpenRewardMenyCoroutine()
    {
        yield return new WaitForSeconds(2f);

        if (!isRewarded)
        {
            game.SetActive(false);
            reward.SetActive(true);
            spawner.SetActive(false);

            isRewarded = true;
        }
        else
        {
            game.SetActive(false);
            retry.SetActive(true);
        }
    }
}
