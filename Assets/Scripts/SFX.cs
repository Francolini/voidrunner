using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public enum Sounds { enemyShoot, playerShoot, explosion, powerUp, laser, none }
    public Sounds sound;

    private AudioSource source;

    public AudioClip enemyShoot, playerShoot, explosion, powerUp, laser;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (sound)
        {
            case Sounds.enemyShoot:
                
                SetSound(enemyShoot, .3f, .57f, .5f, false);

                break;

            case Sounds.playerShoot:

                SetSound(playerShoot, .8f, 1.5f, .5f, false);

                break;

            case Sounds.explosion:

                SetSound(explosion, .5f, .725f, 1f, false);

                break;

            case Sounds.powerUp:

                SetSound(powerUp, .5f, 1, .5f, false);

                break;

            case Sounds.laser:

                SetSound(laser, .6f, 3f, Mathf.Infinity, true);

                break;
        }
    }

    private void SetSound(AudioClip clip, float volume, float pitch, float destroyTime, bool loop)
    {
        sound = Sounds.none;

        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.loop = loop;

        source.Play();

        Destroy(gameObject, destroyTime);
    }
}
