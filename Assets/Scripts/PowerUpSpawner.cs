using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    public GameObject powerUpPrefab, companionsPrefab;

    private bool canSpamPU, canSpamCompanion;

    // Start is called before the first frame update
    void OnEnable()
    {
        canSpamPU = true;
        canSpamCompanion = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canSpamPU)
        {
            StartCoroutine("SpawnPU");
        }

        if (canSpamCompanion)
        {
            StartCoroutine("SpawnCompanions");
        }
    }

    private IEnumerator SpawnPU()
    {
        canSpamPU = !canSpamPU;

        yield return new WaitForSeconds(Random.Range(8, 20));

        GameObject powerUp = Instantiate(powerUpPrefab, new Vector3(transform.position.x + Random.Range(-4, 4.1f), transform.position.y, transform.position.z), transform.rotation);
        canSpamPU = !canSpamPU;
    }

    private IEnumerator SpawnCompanions()
    {
        canSpamCompanion = !canSpamCompanion;

        yield return new WaitForSeconds(Random.Range(10, 25));

        GameObject companions = Instantiate(companionsPrefab, new Vector3(transform.position.x + Random.Range(-4, 4.1f), transform.position.y, transform.position.z), transform.rotation);
        canSpamCompanion = !canSpamCompanion;
    }
}

