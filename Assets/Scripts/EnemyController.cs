using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject bulletPrefab, explosionPrefab, sfxPrefab;

    private GameObject target, gc;
    private PlayerController pc;

    private float speed, health;
    private bool canShoot, dead;

    // Start is called before the first frame update
    void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        gc = GameObject.FindGameObjectWithTag("GameController");

        if(target != null)
        {
            pc = target.GetComponent<PlayerController>();
        }

        speed = Random.Range(3f, 7f);

        if (gameObject.tag.Equals("Enemy"))
        {
            Destroy(gameObject, 6);
        }

        health = 100;

        canShoot = true;
        dead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag.Equals("Enemy"))
        {
            transform.Translate(new Vector2(0, -1) * Time.deltaTime * speed, Space.World);
        }

        if (target != null && canShoot)
        {
            StartCoroutine("Shoot");
        }
    }

    private IEnumerator Shoot()
    {
        canShoot = !canShoot;

        GameObject bullet = Instantiate(bulletPrefab, transform.position, new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z + Random.Range(-0.25f, 0.26f), transform.rotation.w));
        GameObject bulletSound = Instantiate(sfxPrefab, transform.position, transform.rotation);
        bulletSound.GetComponent<SFX>().sound = SFX.Sounds.enemyShoot;

        yield return new WaitForSeconds(Random.Range(.2f, .7f));

        canShoot = !canShoot;
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        CheckHealth();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("PlayerBullet"))
        {
            TakeDamage(Random.Range(35, 75));
            Destroy(collision.gameObject);
        }

        if (collision.tag.Equals("Player") && !collision.gameObject.name.Equals("PlayerTraining"))
        {
            GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(explosion, 2f);

            Destroy(gameObject);
        }
    }

    private void CheckHealth()
    {
        if (health <= 0 && !dead)
        {
            dead = true;

            if (!gc.GetComponent<PrincipalMenu>().canSlow)
            {
                gc.GetComponent<PrincipalMenu>().TurnOnOffSlow();
            }

            GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(explosion, 2f);

            GameObject explosionSound = Instantiate(sfxPrefab, transform.position, transform.rotation);
            explosionSound.GetComponent<SFX>().sound = SFX.Sounds.explosion;

            pc.AddScore(15);
            Destroy(gameObject, .2f);
        }
    }
}

