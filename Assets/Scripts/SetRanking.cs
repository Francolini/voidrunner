using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetRanking : MonoBehaviour
{
    public Text firstPlaceText, secondPlaceText, thirdPlaceText;
    public GameObject scoreGO;

    private TouchScreenKeyboard keyboard;
    
    private int firstSc, secondSc, thirdSc, totalScore, option;
    private string firstString, secondString, thirdString, text;
    private bool done;

    private void OnEnable()
    {
        option = 0;
        done = false;

        LoadScore();
        LoadStrings();

        totalScore = scoreGO.GetComponent<ScoreController>().score;

        if (totalScore >= thirdSc && totalScore != 0)
        {
            keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, "AAA", 3);

            if (totalScore >= firstSc)
            {
                PlayerPrefs.SetInt("third", secondSc);
                PlayerPrefs.SetInt("second", firstSc);
                PlayerPrefs.SetInt("first", totalScore);

                LoadScore();

                option = 1;
            }
            else if (totalScore >= secondSc)
            {
                PlayerPrefs.SetInt("third", secondSc);
                PlayerPrefs.SetInt("second", totalScore);

                LoadScore();

                option = 2;
            }
            else if (totalScore >= thirdSc)
            {
                PlayerPrefs.SetInt("third", totalScore);

                LoadScore();

                option = 3;
            }
            PlayerPrefs.Save();
        }

        ReadText();
    }

    // Update is called once per frame
    void Update()
    {
        if(keyboard.status == TouchScreenKeyboard.Status.Done && !done || keyboard.status == TouchScreenKeyboard.Status.Canceled && !done || keyboard.status == TouchScreenKeyboard.Status.LostFocus && !done)
        {
            done = true;

            text = keyboard.text;
            if(text == "___" || text == "")
            {
                text = ":D: ";
            }

            switch (option)
            {
                case 1:
                    PlayerPrefs.SetString("thirdName", PlayerPrefs.GetString("secondName"));
                    PlayerPrefs.SetString("secondName", PlayerPrefs.GetString("firstName"));
                    PlayerPrefs.SetString("firstName", text);

                    LoadStrings();
                    ReadText();

                    break;

                case 2:
                    PlayerPrefs.SetString("thirdName", PlayerPrefs.GetString("secondName"));
                    PlayerPrefs.SetString("secondName", text);

                    LoadStrings();
                    ReadText();

                    break;

                case 3:
                    PlayerPrefs.SetString("thirdName", text);

                    LoadStrings();
                    ReadText();

                    break;

                default:
                    break;
            }

            text = "";
            PlayerPrefs.Save();
        }
    }

    private void LoadStrings()
    {
        firstString = PlayerPrefs.GetString("firstName");
        secondString = PlayerPrefs.GetString("secondName");
        thirdString = PlayerPrefs.GetString("thirdName");

        if (firstString == "")
        {
            firstString = "___";
            secondString = "___";
            thirdString = "___";
        }
        else if (secondString == "")
        {
            secondString = "___";
            thirdString = "___";
        }
        else if (thirdString == "")
        {
            thirdString = "___";
        }
    }

    private void ReadText()
    {
        firstPlaceText.text = firstString + ": " + firstSc.ToString();
        secondPlaceText.text = secondString + ": " + secondSc.ToString();
        thirdPlaceText.text = thirdString + ": " + thirdSc.ToString();
    }

    private void LoadScore()
    {
        firstSc = PlayerPrefs.GetInt("first");
        secondSc = PlayerPrefs.GetInt("second");
        thirdSc = PlayerPrefs.GetInt("third");
    }
}
