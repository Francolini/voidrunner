using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetRanking : MonoBehaviour
{
    public Text firstPlaceText, secondPlaceText, thirdPlaceText;
    private string firstString, secondString, thirdString;

    private void OnEnable()
    {
        LoadStrings();
        ReadText();     
    }

    private void LoadStrings()
    {
        firstString = PlayerPrefs.GetString("firstName");
        secondString = PlayerPrefs.GetString("secondName");
        thirdString = PlayerPrefs.GetString("thirdName");

        if (firstString == "")
        {
            firstString = "___";
            secondString = "___";
            thirdString = "___";
        }
        else if (secondString == "")
        {
            secondString = "___";
            thirdString = "___";
        }
        else if (thirdString == "")
        {
            thirdString = "___";
        }

        PlayerPrefs.SetString("thirdName", thirdString);
        PlayerPrefs.SetString("secondName", secondString);
        PlayerPrefs.SetString("firstName", firstString);
        PlayerPrefs.Save();
    }

    private void ReadText()
    {
        firstPlaceText.text = PlayerPrefs.GetString("firstName") + ": " + PlayerPrefs.GetInt("first").ToString();
        secondPlaceText.text = PlayerPrefs.GetString("secondName") + ": " + PlayerPrefs.GetInt("second").ToString();
        thirdPlaceText.text = PlayerPrefs.GetString("thirdName") + ": " + PlayerPrefs.GetInt("third").ToString();
    }

    public void ResetRanking()
    {
        PlayerPrefs.DeleteAll();

        LoadStrings();
        ReadText();

        PlayerPrefs.Save();
    }
}
