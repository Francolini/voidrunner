using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedRGB : MonoBehaviour
{

    private Image render;
    public float red, green, blue, colorSpeed;
    public bool changeColor;

    private void Start()
    {
        render = gameObject.GetComponent<Image>();
        red = 1; 
        green = 1;
        blue = 0;
        colorSpeed = 0.1f;

        changeColor = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(changeColor)
        {
            if(red >= 1 && blue <= 0)
            {
                green -= Time.deltaTime / colorSpeed;                
                render.material.SetFloat("_ColorG", green);
            }

            if(red >= 1 && green <= 0)
            {
                blue += Time.deltaTime / colorSpeed;                
                render.material.SetFloat("_ColorB", blue);
            }
            
            if(blue >= 1 && green <= 0)
            {
                red -= Time.deltaTime / colorSpeed;
                render.material.SetFloat("_ColorR", red);
            }

            if(red <= 0 && blue >= 1)
            {
                green += Time.deltaTime / colorSpeed;                
                render.material.SetFloat("_ColorG", green);
            }

            if(red <= 0 && green >= 1)
            {
                blue -= Time.deltaTime / colorSpeed;     
                render.material.SetFloat("_ColorB", blue);
            }
            
            if(blue <= 0 && green >= 1)
            {
                red += Time.deltaTime / colorSpeed;
                render.material.SetFloat("_ColorR", red);
            }
        }
    }
}
