using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class RewardController : MonoBehaviour
{
    public GameObject player, spawner, gameUI, rewardUI, retryUI;

    [SerializeField] private string rewardedId;
    private RewardedAd rewardAd;

    private void Start()
    {
        MobileAds.Initialize(initStatus => { });

        rewardAd = new RewardedAd(rewardedId);

        rewardAd.OnUserEarnedReward += HandleUserEarnedReward;

        AdRequest request = new AdRequest.Builder().Build();
        rewardAd.LoadAd(request);
    }

    public void UserChoseToWatchAd()
    {
        if (rewardAd.IsLoaded())
        {
            rewardAd.Show();
        }
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        StartCoroutine("AdEnded");
    }

    IEnumerator AdEnded()
    {
        yield return new WaitForEndOfFrame();

        gameUI.SetActive(true);
        rewardUI.SetActive(false);
        spawner.SetActive(true);
        player.SetActive(true);
    }

    IEnumerator AdClosed()
    {
        yield return new WaitForEndOfFrame();

        retryUI.SetActive(true);
        rewardUI.SetActive(false);
    }
}
