using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;

    private bool canSpam;

    private float difficultyMin, difficultyMax;

    // Start is called before the first frame update
    void OnEnable()
    {
        canSpam = true;

        difficultyMin = 1f;
        difficultyMax = 8;
    }

    // Update is called once per frame
    void Update()
    {
        if (difficultyMin >= .3f)
        {
            difficultyMin -= Time.deltaTime / 15;
        }

        if (difficultyMax >= .5f)
        {
            difficultyMax -= Time.deltaTime / 6;
        }
        else
        {
            difficultyMax = 5;
        }

        if (canSpam)
        {
            StartCoroutine("SpawnEnemy");
        }
    }

    private IEnumerator SpawnEnemy()
    {
        canSpam = !canSpam;

        yield return new WaitForSeconds(Random.Range(difficultyMin, difficultyMax));

        GameObject enemy = Instantiate(enemyPrefab, new Vector3(transform.position.x + Random.Range(-4, 4.1f), transform.position.y, transform.position.z), transform.rotation);
        canSpam = !canSpam;
    }
}